/* 1. Common Table Expression (CTE): 
 *   - The SubcategorySales CTE calculates the total sales (total_sales) for each product subcategory for the years 1998 to 2001. 
 *     It also uses the LAG window function to compute the sales of the previous year (prev_year_sales) for each subcategory.
 * 2. Main Query: 
 *   - Selects distinct product subcategories where the total_sales for the specified years are consistently higher than the prev_year_sales.
 * 3. Output: 
 *   - The result is a list of distinct product subcategories meeting the specified criteria.
 */

-- 1st Query
WITH SubcategorySales AS (
  SELECT
    p.prod_subcategory,
    EXTRACT(year FROM s.time_id) AS sales_year,
    SUM(s.amount_sold) AS total_sales,
    LAG(SUM(s.amount_sold)) OVER (PARTITION BY p.prod_subcategory ORDER BY EXTRACT(year FROM s.time_id)) AS prev_year_sales
  FROM
    products p
    JOIN sales s ON p.prod_id = s.prod_id
    JOIN times t ON s.time_id = t.time_id
  WHERE
    EXTRACT(year FROM s.time_id) BETWEEN 1998 AND 2001
  GROUP BY
    p.prod_subcategory, EXTRACT(year FROM s.time_id)
)
SELECT DISTINCT
  prod_subcategory
FROM
  SubcategorySales
WHERE
  total_sales > COALESCE(prev_year_sales, 0);

 
/* 1. Common Table Expression (CTE): 
 *   - Similar to the first query, the SubcategorySales CTE calculates the total sales (total_sales) for each product subcategory for the years 
 *     1998 to 2001. It also computes the sales of the previous year (prev_year_sales) using the LAG window function.
 * 2. Main Query: 
 *   - Selects columns for prod_subcategory, sales_year, total_sales, and prev_year_sales from the SubcategorySales CTE.
 * 3. Output: 
 *   - The result is a table that includes information on product subcategories, sales years, total sales, and previous year sales.
 */
 
-- 2nd Query
WITH SubcategorySales AS (
  SELECT
    p.prod_subcategory,
    EXTRACT(year FROM s.time_id) AS sales_year,
    SUM(s.amount_sold) AS total_sales,
    LAG(SUM(s.amount_sold)) OVER (PARTITION BY p.prod_subcategory ORDER BY EXTRACT(year FROM s.time_id)) AS prev_year_sales
  FROM
    products p
    JOIN sales s ON p.prod_id = s.prod_id
    JOIN times t ON s.time_id = t.time_id
  WHERE
    EXTRACT(year FROM s.time_id) BETWEEN 1998 AND 2001
  GROUP BY
    p.prod_subcategory, EXTRACT(year FROM s.time_id)
)
SELECT
  prod_subcategory,
  sales_year,
  total_sales,
  prev_year_sales
FROM
  SubcategorySales
WHERE
  total_sales > COALESCE(prev_year_sales, 0);
  
 -- The key difference is that the 2nd response query provides more details in the output by including the sales information for each subcategory, allowing for a more comprehensive analysis.
 
 
 
 